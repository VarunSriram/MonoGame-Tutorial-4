﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameAdvancePrep
{
    public class Sprite
    {
        private Color color;
        private Vector2 velocity;
        private Vector2 position;
        private Rectangle rectangle;
        private Texture2D texture; 

        public Sprite(Vector2 pos, float speed = 0, float angle = 0)
        {
            this.position = pos;
            this.velocity = new Vector2((float)(speed * Math.Cos(angle)), (float)(speed * Math.Sin(angle)));
            this.texture = null;
            this.color = Color.White;
        }

        protected Texture2D Texture => texture;

        public Vector2 Position => position;

        public Rectangle Rectange => rectangle;

        public bool Collided { get; private set; }

        public void loadContent(ContentManager content, GraphicsDevice graphicDevice, string assetName)
        {
            texture = content.Load<Texture2D>(assetName);
            OnContentLoaded(content, graphicDevice);
        }

        //Method to do something once Content is loaded.
        protected virtual void OnContentLoaded(ContentManager content, GraphicsDevice graphicsDevice)
        {
            UpdateRectangle();
        }

        private void UpdateRectangle()
        {
            rectangle = new Rectangle((int)position.X, (int)position.Y,texture.Width, texture.Height);

        }

        public virtual void Unload()
        {
            texture.Dispose();
        }

        public void Update(GameTime gameTime)
        {
            position += velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
            UpdateRectangle();
        }
        //Assume Begin was already called when using this mehtod
        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(texture, position, color);
        }
    }
}
